$(function () {
  /* When a video modal closes */
  $('.js-video-modal').on('hidden.bs.modal', () => {
    // Find its video and pause it
    const _video = $(this).find('video')[0]
    _video.pause()
  })
})
